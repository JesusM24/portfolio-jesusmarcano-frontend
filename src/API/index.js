import axios from 'axios';

// *********** Send email
export const SendEmail = async ({
    name,
    email,
    message,
    setSend,
  }) => {
    try {
      const data = { name, email, message };
      let res = await axios.post(`https://portfolio-jesusmarcano-backend.uc.r.appspot.com/send`, data);
      if (res) {
        setSend(res.data);
      }
    } catch (error) {
      alert(error.response.data.message);
    }
  };