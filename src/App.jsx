import React from 'react';


//Components
import Navbar from "./Pages/Components/Navbar";
import Home from "./Pages/Components/Home"
import Proyectos from "./Pages/Components/Proyectos";
import Skills from "./Pages/Components/Skills";
import Form from "./Pages/Components/Form/Form";
import Footer from "./Pages/Components/Footer";
import About from "./Pages/Components/About";

function App() {
  return (
    <>
      <Navbar/>
      <Home/>
      <About/>
      <Proyectos/>
      <Skills/>
      <Form/>
      <Footer/>
    </>
  );
}

export default App;