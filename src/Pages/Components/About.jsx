import React, {useState} from "react";
import {Row, Col, Container } from 'reactstrap';
import { CSSTransition } from "react-transition-group";
//Card
import {Study , Personal, Idiomas} from "./Card/card";

const About = () => {

    const [showFront, setShowFront] = useState({
        card1: true,
        card2: true,
        card3: true,
    });

    const toggleElement = (modal) => {
        const mdl = { ...showFront };
        mdl[modal] = !mdl[modal];
        setShowFront(mdl);
    };

    return(
        <section id="About">
            <Container>
                <Row className="g-0" data-aos="fade-up" data-aos-duration="1500">
                    <Col className="d-flex justify-content-center" data-aos="fade-up" data-aos-duration="1000">
                        <h2 className="aboutTitle">SOBRE MI</h2>
                    </Col>
                </Row>
                <Row className="g-0 pt-5" data-aos="zoom-in-up" data-aos-duration="1000">
                        <Col lg="12" className="d-flex w-100 justify-content-center">
                            <h3 className="text-center text-banner">
                                Soy un programador Junior en React.Js, con uso de ReactSrap,<br />
                                Bootstrap, Saas, consumo de API REST y testing con Postman,<br />
                                soy capaz de crear sitios web dinámicos.
                            </h3>
                        </Col>
                </Row>
                <Row className="py-5 g-0">
                    <Col data-aos="zoom-in-up" data-aos-duration="1000">
                        <div className="flippable-card-container mx-auto">
                        <CSSTransition
                        in={showFront.card1}
                        timeout={1000}
                        classNames='flip'
                        >
                            <Study onClick={() => toggleElement(`card1`)}/>
                        </CSSTransition>
                        </div>
                    </Col>

                    <Col data-aos="zoom-in-up" data-aos-duration="2000">
                        <div className="flippable-card-container mx-auto">
                        <CSSTransition
                        in={showFront.card2}
                        timeout={1000}
                        classNames='flip'
                        >
                            <Personal onClick={() => toggleElement(`card2`)}/>
                        </CSSTransition>
                        </div>
                    </Col>

                    <Col data-aos="zoom-in-up" data-aos-duration="3000">
                        <div className="flippable-card-container mx-auto">
                        <CSSTransition
                        in={showFront.card3}
                        timeout={1000}
                        classNames='flip'
                        >
                            <Idiomas onClick={() => toggleElement(`card3`)}/>
                        </CSSTransition>
                        </div>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}

export default About;