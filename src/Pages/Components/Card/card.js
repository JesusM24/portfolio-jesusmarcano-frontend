export function Study({onClick}) {
    return(
        <div className="card" onClick={onClick}>
            <div className="display-flex card-back">
                <div>
                    <h3 className="card-back-title text-center">
                        Primarios
                    </h3>

                    <div className="text-center textCard">
                        <p className=" card-back-content">
                            Colegio Nuestra Señora de Fàtima.
                        </p>

                        <p className=" card-back-content">
                            I.E Juana Infantes Vera.
                        </p>
                    </div>

                    <h3 className="card-back-title text-center">
                        Secundarios
                    </h3>

                    <div className="text-center">
                        <p className=" card-back-content">
                            Instituto CIBERTEC (Cursando 2do Ciclo)
                        </p>
                    </div>
                </div>
            </div>
            <div className="card-front1"></div>
        </div> 
    )
}

export function Personal({onClick}) {
    return(
        <div className="card" onClick={onClick}>
            <div className="display-flex card-back">
                <div class="text-center">
                    <h3 className=" card-back-title">Nombre:</h3>
                    <p>
                        Jesús Francisco Marcano Abreu
                    </p>

                    <h3 className="card-back-title">Edad:</h3>
                    <p>
                        21 años
                    </p>

                    <h3 className="card-back-title">Nacimiento:</h3>
                    <p className="m-0">
                        24/09/2001
                    </p>
                </div>
            </div>
            <div className="card-front2">
            </div>
        </div> 
    )
}

export function Idiomas({onClick}) {
    return(
        <div className="card" onClick={onClick}>
            <div className="display-flex card-back">
                <div>
                    <p className="text-center">
                        <h3 className="card-back-title">Español:</h3> Nativo
                    </p>

                    <p className="text-center">
                        <h3 className="card-back-title">Ingles:</h3> Intermedio
                    </p>

                    <p className="text-center m-0">
                        <h3 className="card-back-title">Francés:</h3> Básico
                    </p>
                </div>
            </div>
            <div className="card-front3"></div>
        </div> 
    )
}