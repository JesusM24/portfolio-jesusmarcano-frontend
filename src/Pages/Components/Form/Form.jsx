import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Col } from "reactstrap";
import { SendEmail } from "../../../API";
import InlineError from "./InlineError";
import { validateEmail, validateMessage, validateName } from "./validation";
import {Toaster , toast} from 'react-hot-toast';

const Form = () => {

    const [name,setName] = useState("");
    const [email,setEmail] = useState("");
    const [message,setMessage] = useState("");
    const [nameError,setNameError] = useState();
    const [emailError,setEmailError] = useState();
    const [messageError,setMessageError] = useState();
    const [buttonLoading, setButtonLoading] = useState(false);
    const [send, setSend] = useState();


    useEffect(() =>{
        // ********** VALIDATION **********
        validateName({name,setNameError});
        validateEmail({email,setEmailError});
        validateMessage({message,setMessageError});

    /************ */
    if (send) {
        toast.success(send.msg)
        setName("");
        setEmail("");
        setMessage("");
        setSend()
    }

    },[name, email, message, send]);


    const submitHandler = (e) => {
        e.preventDefault();
        setButtonLoading(true)

        if (!nameError & !emailError & !messageError) {
            SendEmail({
                name,
                email,
                message,
                setSend,}).then(() =>{
                setButtonLoading(false)
                })
        } else {
            setButtonLoading(false)
        }
    };

    return(
        <section id="formSection">

            <svg preserveAspectRatio="none" viewBox="0 0 100 102" height="75" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" className="svgcolor-light ovo">
                <path d="M0 0 L50 100 L100 0 Z" fill="#EFFCEF" stroke="transparent"></path>
            </svg>

            <div className="d-flex justify-content-center" data-aos="fade-up" data-aos-duration="1000">
                <h2 className="title">CONTACTO</h2>
            </div>

            <div className="d-flex justify-content-center" data-aos="fade-up" data-aos-duration="1300">
                <h3 className="text-center font-weight-bold py-2">Te interesa mi perfil? ¡Trabajemos juntos!</h3>
            </div>

            <form onSubmit={submitHandler} className="form" data-aos="fade-up" data-aos-duration="1500">

                <div>
                    <div className="formContainer">
                            <Col className="d-flex justify-content-center label">
                                {/* Name */}
                                <label>Nombre</label>
                            </Col>

                            <Col className="d-flex justify-content-center my-2 position-relative">

                                <input 
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    className="name p-3 mb-2"
                                    required
                                    type="text"
                                    placeholder="nombre..."
                                />
                                    {nameError && <InlineError error={nameError} /> }
                            </Col>

                            <Col className="d-flex justify-content-center label">
                                {/* e-mail */}
                                <label>Correo</label>
                            </Col>

                            <Col className="d-flex justify-content-center my-2 position-relative">
                                <input 
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    className="email p-3 mb-2"
                                    required
                                    type="text"
                                    placeholder="alguien@ejemplo.com"
                                />
                                    {emailError && <InlineError error={emailError} /> }
                            </Col>

                            <Col className="d-flex justify-content-center label">
                                {/* Mensaje */}
                                <label>Mensaje</label>
                            </Col>

                            <Col className="d-flex justify-content-center my-2 position-relative">
                                <textarea
                                    value={message}
                                    onChange={(e) => setMessage(e.target.value)}
                                    name="Mensaje" 
                                    placeholder="Escribe un mensaje...">
                                </textarea>
                            </Col>

                            <Col className="d-flex justify-content-center" >
                                <button
                                    type="submit"
                                    className="buttonForm"
                                    disabled={buttonLoading && true}>
                                        {buttonLoading ? "Cargando..." : "Enviar"}
                                </button>
                            </Col>
                    </div>
                </div>
            </form>

            <Toaster
                position="top-right"
                outoClose={2000}/>
        </section>
    )
}

export default Form;