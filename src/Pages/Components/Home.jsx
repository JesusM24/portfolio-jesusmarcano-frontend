import React from "react";
import { Row, Col, Container } from 'reactstrap';
import gitlab from '../Assets/Social-Media/gitlab.png'
import Linkedin from '../Assets/Social-Media/linkedin.png'
import whatsapp from '../Assets/Social-Media/whatsapp.png'
import iconDown from "../Assets/svg/iconDown.svg"
/* import { Parallax } from 'react-scroll-parallax'; */

import hill1 from "../Assets/Parallax/hill1.png"
import hill2 from "../Assets/Parallax/hill2.png"
import hill3 from "../Assets/Parallax/hill3.png"
import hill4 from "../Assets/Parallax/hill4.png"
import hill5 from "../Assets/Parallax/hill5.png"
import leaf from "../Assets/Parallax/leaf.png"
import plant from "../Assets/Parallax/plant.png"
import tree from "../Assets/Parallax/tree.png"

import { Parallax } from "react-scroll-parallax";

const Home = () => {

    return(
        <section id="Home">
            <Container className="homeContainer">
                    <div className="contentWrapper">
                        <div>
                        <Row className="g-0" data-aos="zoom-in-up" data-aos-duration="800">
                            <Col className="text-center">
                                <h4>PORTAFOLIO</h4>
                                <h1 className="presentation">JESUS MARCANO</h1>
                            </Col>
                        </Row>
                        <Row className="g-0">
                            <Col>
                                <div className="socialMedia d-flex w-100 justify-content-center">
                                    <a className="gitlab" href="https://gitlab.com/JesusM24" target="blank" data-aos="fade-up" data-aos-duration="1500">
                                        <img src={gitlab} alt="Gitlab" />
                                    </a>

                                    <a  className="linkedin" href="https://www.linkedin.com/in/jes%C3%BAs-marcano-117b7024a/" target="blank" data-aos="fade-up" data-aos-duration="1800">
                                        <img  src={Linkedin} alt="Linkedin" />
                                    </a>

                                    <a className="whatsapp" href="https://wa.link/8v62u9" target="blank" data-aos="fade-up" data-aos-duration="2000">
                                        <img src={whatsapp} alt="whatsapp" />
                                    </a>
                                </div>

                                <a href="#About" data-aos="fade-up" data-aos-duration="2500" className="d-flex w-100 justify-content-center">
                                    <img className="iconDown" src={iconDown} alt="icon" />
                                </a>
                            </Col>
                        </Row>
                        </div>
                    </div>


                <div className="parallaxContainer">
                    <Parallax speed={-20} className="Parallax h-100">
                        <img src={hill1} id="img1" alt="img1" className="h-100"/>
                    </Parallax>                    
                    <Parallax speed={-15} className="Parallax h-100">
                        <img src={hill2} alt="img2" className="h-100"/>
                    </Parallax>
                    <img src={hill3} alt="img3" className="Parallax h-100"/>
                    <Parallax translateX={[18,-20]} className="Parallax h-100">
                        <img src={hill4} alt="img4" className="h-100"/>
                    </Parallax>
                    <Parallax translateX={[-18,20]} className="Parallax2 h-100">
                        <img src={hill5} alt="img5" className="h-100"/>
                    </Parallax>

                    <Parallax translateX={[-20,30]} speed={10} className="Parallax2 h-100">
                        <img src={leaf} alt="img6" className="h-100"/>
                    </Parallax>
                    <img src={plant} alt="img7" className="Parallax h-100"/>
                    <img src={tree} alt="img8" className="Parallax h-100"/>
                </div>
            </Container>
        </section>
    )
}

export default Home;