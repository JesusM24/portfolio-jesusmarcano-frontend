import React, {useState} from "react";
import { Container,Row, Col } from "reactstrap";

//logo
import logo from "../Assets/logo/LogoJM.png";

//svg
import barsBlack from "../Assets/icons/bars-solid-black.svg"

//Social media
import gitlab from '../Assets/svg/gitlab.svg'
import Linkedin from '../Assets/svg/linkedin.svg'
import whatsapp from '../Assets/svg/whatsapp.svg'


const Navbar = () => {
  
  const [active, setActive] = useState({
    btn1: false,
    btn2: false,
    btn3: false,
    btn4: false,
  });

  const [burguer , setBurguer] = useState(false);

  const toggleElement = (modal) => {
    const mdl = { ...active };
    Object.keys(mdl).forEach((item) => {
      if (mdl[item] === true) {
        mdl[item] = false;
      }
    });
    mdl[modal] = !mdl[modal];
    setActive(mdl);
  };

  return (
    <section id="Navbar">
        <div className="navbarContainer">
          <Container>
            <Row className="g-0 d-flex">
              <Col xs="7" className="d-flex">
                <div className="logoContainer">
                  <img src={logo} alt="Logo" className="logo" />
                </div>

                <div className="nameContainer">
                  <span className="disapear">Jesús Marcano</span>
                  <span className="disapear"> - Frontend Developer</span>
                </div>
              </Col>

              <Col xs="5">
                <nav className={`navigation ${burguer ? "burguerActive" : ""}`}>
                  <ul className="navList d-flex">
                    <li onClick={() => {toggleElement(`btn1`) ; setBurguer(!burguer)}}>
                      <a 
                        className={`${active.btn1 ? "active" : ""}`} 
                        href="#About">
                        About
                      </a>
                    </li>
                    <li onClick={() => {toggleElement(`btn2`) ; setBurguer(!burguer)}}>
                      <a
                        className={`${active.btn2 ? "active" : ""}`}
                        href="#Proyectos">
                        Portfolio
                      </a>
                    </li>
                    <li onClick={() => {toggleElement(`btn3`) ; setBurguer(!burguer)}}>
                      <a 
                      className={`${active.btn3 ? "active" : ""}`} 
                      href="#skills">
                        Skills</a>
                    </li>
                    <li onClick={() => {toggleElement(`btn4`) ; setBurguer(!burguer)}}>
                      <a 
                      className={`${active.btn4 ? "active" : ""}`}
                      href="#formSection">
                      Contacto</a>
                    </li>
                    <div className="socialMedia d-flex w-100 justify-content-center">
                      <a href="https://gitlab.com/JesusM24" target="blank">
                          <img className="gitlab" src={gitlab} alt="Gitlab" />
                      </a>

                      <a href="https://www.linkedin.com/in/jes%C3%BAs-marcano-117b7024a/" target="blank">
                          <img className="linkedin" src={Linkedin} alt="Linkedin" />
                      </a>

                      <a href="https://wa.link/8v62u9" target="blank">
                          <img src={whatsapp} alt="whatsapp" />
                      </a>
                      </div>
                  </ul>
                </nav>
              </Col>

              <Col sm="1" className="bars">
                <img
                onClick={() => setBurguer(!burguer)}
                src={barsBlack}
                className="bars" 
                alt="bars" />
              </Col>
            </Row>
          </Container>
        </div>
    </section>
  );
};

export default Navbar;
