import React from "react";
import { Row, Col} from 'reactstrap';
import rootStudio from '../Assets/Proyects/root-studio.PNG';
import pokedex from '../Assets/Proyects/pokedex.PNG';

const Proyectos = () => {
    return(
        <section id="Proyectos" className="px-5">
                <Row className="g-0" data-aos="fade-up" data-aos-duration="1500">
                    <Col className="d-flex justify-content-center" data-aos="fade-up" data-aos-duration="1000">
                        <h2 className="proyectosTitle">PROYECTOS</h2>
                    </Col>
                </Row>

                <Row>
                    <Col lg="6" className="g-0 rootStudio" data-aos="fade-right" data-aos-duration="2000">
                        <div className="rootContainer">
                            <a className="d-flex justify-content-center" 
                            href="https://root-studio.netlify.app/"
                            target="blank">
                                <img className="imgProyect" src={rootStudio} alt="" />
                            </a>
                        </div>
                    </Col>

                    <Col lg="6" className="rootProyect" data-aos="fade-left" data-aos-duration="2000">
                        <div>
                            <div className="textProyect">
                                <div className="d-flex justify-content-center">
                                    <h3 className="proyectTitle">ROOT-STUDIO</h3>
                                </div>

                                <div>
                                    <p className="text-center">
                                        Este proyecto fue programado con React.js, bootstrap, Reactstrap y Saas. Además, posee responsive design. 
                                    </p>
                                </div>
                            </div>

                            <div className="d-flex justify-content-center pt-2 pb-5">
                                <a
                                target="blank" 
                                href="https://root-studio.netlify.app/">
                                    <button className="buttonProbar">
                                        Probar
                                    </button>
                                </a>

                                <a
                                target="blank" 
                                href="https://gitlab.com/JesusM24/root-studio">
                                    <button className="buttonCodigo">
                                        Ir al código
                                    </button>
                                </a>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row className="g-0 pokedex">
                    <Col lg="6" className="pokeText" data-aos="fade-right" data-aos-duration="2000">
                        <div>
                            <div className="textProyect">
                                <div className="d-flex justify-content-center">
                                    <h3 className="proyectTitle">POKEDEX</h3>
                                </div>

                                <div>
                                    <p className="text-center">
                                    En este proyecto logré consumir la API de pokémon, ordenando de manera ascendente todos los pokemones existentes, usando HTML, 
                                    CSS, Javascript, Fetch y Paginación.
                                    </p>
                                </div>
                            </div>

                            <div className="d-flex justify-content-center">
                                <a
                                target="blank" 
                                href="https://myfirstpokedex1.netlify.app/">
                                    <button className="buttonProbar">
                                        Probar
                                    </button>
                                </a>

                                <a
                                target="blank" 
                                href="https://gitlab.com/JesusM24/api-pokemon">
                                    <button className="buttonCodigo">
                                        Ir al código
                                    </button>
                                </a>
                            </div>
                        </div>
                    </Col>

                    <Col lg="6" data-aos="fade-left" data-aos-duration="2000">
                        <div className="pokedexContainer">
                            <a 
                            className="d-flex justify-content-center" 
                            href="https://myfirstpokedex1.netlify.app/"
                            target="blank">
                                <img className="imgProyect" src={pokedex} alt="pokedex" />
                            </a>
                        </div>
                    </Col>
                </Row>
        </section>
    )
}

export default Proyectos;