import React from "react";
import {Row, Col} from "reactstrap";
import html from "../Assets/svg/html.svg";
import css from "../Assets/svg/css.svg";
import javascript from "../Assets/svg/javascript.svg";
import react from "../Assets/svg/react.svg"
import bootstrap from "../Assets/svg/bootstrap5.svg"
import sass from "../Assets/svg/sass.svg"
import postman from "../Assets/svg/postman.svg"
import mysql from "../Assets/svg/mysql.svg"
import node from "../Assets/svg/nodejs.svg"
import gitlab from "../Assets/svg/gitlab.svg"
import java from "../Assets/svg/java.svg"

const Skills = () => {
    return(
        <section id="skills">

            <Row className="g-0">
                <Col lg="12" className="d-flex justify-content-center" data-aos="fade-up" data-aos-duration="1000">
                    <h2 className="skillsTitle">SKILLS</h2>
                </Col>
            </Row>

            <Row className="g-0" data-aos="zoom-in-up" data-aos-duration="1500">
                <Col xs="10" sm="9" xl="8" className="mx-auto">

                    <div className="d-flex flex-wrap justify-content-center skillsWrapper" data-aos="fade-up" data-aos-duration="1000">

                        <div className="skill html">
                            <img className="svg" src={html} alt="html logo" />
                        </div>     

                        <div className="skill css">
                            <img className="svg" src={css} alt="css logo" />
                        </div>

                        <div className="skill js">
                            <img className="svg" src={javascript} alt="javascript logo" />
                        </div>

                        <div className="skill react">
                            <img className="svg" src={react} alt="react logo" />
                        </div>

                        <div className="skill bootstrap">
                            <img className="svg" src={bootstrap} alt="bootstrap logo" />
                        </div>

                        <div className="skill sass">
                            <img className="svg" src={sass} alt="saas logo" />
                        </div>
                        
                        <div className="skill postman">
                            <img className="svg" src={postman} alt="postman logo" />
                        </div>

                        <div className="skill mysql">
                            <img className="svg" src={mysql} alt="MySql logo" />
                        </div>

                        <div className="skill node">
                            <img className="svg" src={node} alt="node logo" />
                        </div>

                        <div className="skill gitlab">
                            <img className="svg" src={gitlab} alt="gitlab logo" />
                        </div>

                        <div className="skill java">
                            <img className="svg" src={java} alt="java logo" />
                        </div>
                    </div>
                </Col>
            </Row>
        </section>
    )
}

export default Skills;